﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControl : MonoBehaviour 
{

	public float speed = 3.0f;
	public float rotate = 250.0f;

	public GameObject Ship;
	public GameObject Rocket;

	void Start () 
	{
		GameManager.instance.spawnedPlayer = this.gameObject;
	}

	void Update()
	{
		//Adding rotation controls
		transform.Rotate (0, 0, - Input.GetAxis ("Horizontal") * rotate * Time.deltaTime);
		//adding movement controls
		GetComponent<Rigidbody2D> ().AddForce (transform.up * speed * Input.GetAxis ("Vertical"));
		//adding firing functionality
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			FireRocket ();
		}
	}

	void OnDestroy()
	{
		
		Destroy(GameManager.instance.EnemiesSpawned[0]);
		Destroy(GameManager.instance.EnemiesSpawned[1]);
		Destroy(GameManager.instance.EnemiesSpawned[2]);
	}

	void FireRocket()
	{
		Instantiate (Rocket, new Vector3 (transform.position.x, transform.position.y, 0), transform.rotation);
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject.tag != "bullet" && c.gameObject.tag != "boundary") 
		{
			Debug.Log ("Actor who destroyed me: " + c.gameObject.name);
			Destroy (gameObject);
			GameManager.instance.DeIncrementLives ();
		}
	}
}