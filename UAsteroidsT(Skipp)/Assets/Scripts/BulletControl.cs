﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControl : MonoBehaviour 
{
	public int timer;

	// Use this for initialization
	void Start () 
	{
		Destroy (gameObject, timer);
		GetComponent<Rigidbody2D> ().AddForce (transform.up * 400);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
