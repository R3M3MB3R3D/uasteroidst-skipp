﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidControl : MonoBehaviour 
{
	[Range(0,360)]
	public float enemyRotationSpeed;
	private Transform player;
	public bool tracking;
	public int moveSpeed;

	void Start ()
	{
		GameManager.instance.EnemiesSpawned.Add(this.gameObject);

		if (GameManager.instance.spawnedPlayer) 
		{
			player = GameManager.instance.spawnedPlayer.transform as Transform;
		}
		if (player) 
		{
			if ((this.gameObject.GetComponent<CanTrack> ().canTrackTarget == false) && (player)) 
			{
				noHoming ();
			}
		}
	}

	void Update ()
	{
		if ((this.gameObject.GetComponent<CanTrack>().canTrackTarget == true) && (player)) 
		{
			yesHoming ();
		}
		if (player == null) 
		{
			player = GameManager.instance.spawnedPlayer.transform as Transform;
		}
	}

	void OnDestroy()
	{
		GameManager.instance.EnemiesSpawned.Remove (this.gameObject);
	}

	//create a function for destroying the asteroids
	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject.tag.Equals ("bullet"))
		{
			//destroys the rocket
			Destroy (c.gameObject);
			//destroys the asteroid
			Destroy(gameObject);
		}
		if (c.gameObject.tag.Equals ("Player")) 
		{
			//destroys the player
			Destroy (c.gameObject);
			//destroys the asteroid
			Destroy(gameObject);
		}
	}

	//creating functions for movement based on player location
	void noHoming ()
	{
		Vector3 direction = player.position - transform.position;
		direction.Normalize ();
		float zAngle = (Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg - 90);
		transform.rotation = Quaternion.Euler (0, 0, zAngle);
		GetComponent<Rigidbody2D> ().AddForce (transform.up * 100);
	}

	void yesHoming()
	{
		Vector3 direction = player.position - transform.position;
		Debug.Log (transform.position);
		direction.Normalize ();
		float zAngle = (Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg - 90);
		Quaternion targetLocation = Quaternion.Euler (0, 0, zAngle);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, targetLocation, enemyRotationSpeed * Time.deltaTime);
		transform.Translate (0, moveSpeed * Time.deltaTime,0);
	}
}