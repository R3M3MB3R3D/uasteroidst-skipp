﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{

	public static GameManager instance;
	public GameObject Asteroid;
	public GameObject Ship;
	public GameObject Rocket;
	public GameObject Boundary;
	public GameObject spawnedPlayer;

	public List<GameObject> Enemies;
	public List<Transform> spawnPoints;
	public List<GameObject> EnemiesSpawned;

	public int playerLives;

	void Awake()
	{
		if (instance == null) 
		{
			instance = this;
			DontDestroyOnLoad (gameObject);
		} 
		else 
		{
			Destroy (this.gameObject);
		}
	}

	void Start ()
	{
		BeginGame ();
	}

	void Update ()
	{
		if (Input.GetKey (KeyCode.Escape))
			Application.Quit ();
		SpawnPlayer ();
		SpawnAsteroids ();
	}

	void BeginGame()
	{
		playerLives = 3;
	}

	void SpawnPlayer()
	{
		if (spawnedPlayer == null && playerLives != 0)
		{
			Instantiate (Ship, new Vector3 (0,0,0), Quaternion.Euler(0,0,0));
		}
	}
		
	public void DeIncrementLives()
	{
		playerLives--;
		if (playerLives > 0) 
		{
			Instantiate (Ship, new Vector3 (0,0,0), Quaternion.Euler(0,0,0));
		}
		if (playerLives < 1) 
		{
			BeginGame ();
		}
	}

	void SpawnAsteroids()
	{

		if (EnemiesSpawned.Count <= 2)
		{
			int randomSpawnPoint = Random.Range (0, spawnPoints.Count);
			int randomNum = Random.Range(0,Enemies.Count);
			Vector3 spawnNearby = Random.insideUnitCircle;
			Vector3 spawnLocation = spawnPoints[randomSpawnPoint].position + spawnNearby;
			Instantiate (Enemies[randomNum], spawnLocation , spawnPoints[randomSpawnPoint].rotation); // Need to use Random.insideUnitCircle
		}
	}
		
	public void DeIncrementAsteroids()
	{
		SpawnAsteroids ();
	}
}