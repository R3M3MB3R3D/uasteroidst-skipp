﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryControl : MonoBehaviour 
{
		void Start()
		{
			GameManager.instance.Boundary = this.gameObject;
		}

		void OnTriggerExit2D (Collider2D other)
		{
			if (other.gameObject.tag == "Player") 
			{
				Destroy (other.gameObject);
				GameManager.instance.DeIncrementLives ();
			}

		if (other.gameObject.tag == "Enemy") 
		{
			Destroy (other.gameObject);
			GameManager.instance.DeIncrementAsteroids ();
		}
	}
}
